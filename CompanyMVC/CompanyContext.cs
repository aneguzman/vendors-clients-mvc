﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using CompanyMVC.Domain;

namespace CompanyMVC
{
    public class CompanyContext : DbContext
    {
        public CompanyContext() : base("MyCompanyDB")
        {
            
        }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<Customer> Customers { get; set; }
    }
}