// <auto-generated />
namespace CompanyMVC.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class TakingOffNameFromVendor : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(TakingOffNameFromVendor));
        
        string IMigrationMetadata.Id
        {
            get { return "201607261244045_TakingOffNameFromVendor"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
