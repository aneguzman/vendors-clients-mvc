namespace CompanyMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TakingOffNameFromVendor : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Vendors", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Vendors", "Name", c => c.String());
        }
    }
}
