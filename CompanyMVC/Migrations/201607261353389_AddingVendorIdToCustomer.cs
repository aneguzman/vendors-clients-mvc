namespace CompanyMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingVendorIdToCustomer : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Customers", "Vendor_VendorId", "dbo.Vendors");
            DropIndex("dbo.Customers", new[] { "Vendor_VendorId" });
            RenameColumn(table: "dbo.Customers", name: "Vendor_VendorId", newName: "VendorId");
            AlterColumn("dbo.Customers", "VendorId", c => c.Int(nullable: false));
            CreateIndex("dbo.Customers", "VendorId");
            AddForeignKey("dbo.Customers", "VendorId", "dbo.Vendors", "VendorId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Customers", "VendorId", "dbo.Vendors");
            DropIndex("dbo.Customers", new[] { "VendorId" });
            AlterColumn("dbo.Customers", "VendorId", c => c.Int());
            RenameColumn(table: "dbo.Customers", name: "VendorId", newName: "Vendor_VendorId");
            CreateIndex("dbo.Customers", "Vendor_VendorId");
            AddForeignKey("dbo.Customers", "Vendor_VendorId", "dbo.Vendors", "VendorId");
        }
    }
}
