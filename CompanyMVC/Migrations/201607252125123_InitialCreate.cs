namespace CompanyMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        CompanyId = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(),
                    })
                .PrimaryKey(t => t.CompanyId);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerId = c.Int(nullable: false, identity: true),
                        Account = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        Company_CompanyId = c.Int(),
                        Vendor_VendorId = c.Int(),
                    })
                .PrimaryKey(t => t.CustomerId)
                .ForeignKey("dbo.Companies", t => t.Company_CompanyId)
                .ForeignKey("dbo.Vendors", t => t.Vendor_VendorId)
                .Index(t => t.Company_CompanyId)
                .Index(t => t.Vendor_VendorId);
            
            CreateTable(
                "dbo.Vendors",
                c => new
                    {
                        VendorId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        Company_CompanyId = c.Int(),
                    })
                .PrimaryKey(t => t.VendorId)
                .ForeignKey("dbo.Companies", t => t.Company_CompanyId)
                .Index(t => t.Company_CompanyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Customers", "Vendor_VendorId", "dbo.Vendors");
            DropForeignKey("dbo.Vendors", "Company_CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Customers", "Company_CompanyId", "dbo.Companies");
            DropIndex("dbo.Vendors", new[] { "Company_CompanyId" });
            DropIndex("dbo.Customers", new[] { "Vendor_VendorId" });
            DropIndex("dbo.Customers", new[] { "Company_CompanyId" });
            DropTable("dbo.Vendors");
            DropTable("dbo.Customers");
            DropTable("dbo.Companies");
        }
    }
}
