namespace CompanyMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TakingOffCompanyField : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Customers", "Company_CompanyId", "dbo.Companies");
            DropForeignKey("dbo.Vendors", "Company_CompanyId", "dbo.Companies");
            DropIndex("dbo.Customers", new[] { "Company_CompanyId" });
            DropIndex("dbo.Vendors", new[] { "Company_CompanyId" });
            DropColumn("dbo.Customers", "Company_CompanyId");
            DropColumn("dbo.Vendors", "Company_CompanyId");
            DropTable("dbo.Companies");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Companies",
                c => new
                    {
                        CompanyId = c.Int(nullable: false, identity: true),
                        CompanyName = c.String(),
                    })
                .PrimaryKey(t => t.CompanyId);
            
            AddColumn("dbo.Vendors", "Company_CompanyId", c => c.Int());
            AddColumn("dbo.Customers", "Company_CompanyId", c => c.Int());
            CreateIndex("dbo.Vendors", "Company_CompanyId");
            CreateIndex("dbo.Customers", "Company_CompanyId");
            AddForeignKey("dbo.Vendors", "Company_CompanyId", "dbo.Companies", "CompanyId");
            AddForeignKey("dbo.Customers", "Company_CompanyId", "dbo.Companies", "CompanyId");
        }
    }
}
